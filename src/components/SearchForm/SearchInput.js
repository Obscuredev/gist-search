import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  input: {
    margin: theme.spacing.unit,
  },

  
});

function SearchInput(props) {

    const { classes } = props;

    return (

        <div className={classes.container}>
            <Grid container spacing={16}>
                <Grid item xs={ 10 }>
                    <Input
                        id="search-username"
                        placeholder="Enter a github username"
                        className={classes.input}
                        inputProps={{
                            'aria-label': 'Description',
                        }}
                        autoFocus={ true } 
                        fullWidth={ true }
                    />
                </Grid>
                <Grid item xs={ 2 }>
                    <Button variant="contained" onClick={ props.validateInput } color="secondary" fullWidth={ true } className={ styles.button } >
                        Search
                    </Button>
                </Grid>
            </Grid>
        </div>

        
    );
}

SearchInput.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchInput);