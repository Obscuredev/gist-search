import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import SearchInputComponent from './SearchInputComponent';


const styles = theme => ({
    paper: {
      padding: theme.spacing.unit * 4,
      marginTop: theme.spacing.unit*4,
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    button: {
  
      margin: theme.spacing.unit,
      borderRadius: theme.spacing.unit
  
    }
});

function SearchForm( props ){

    const { classes } = props;

    return(
        <Grid item xs={12} md={ 9 }>
            <Paper className={classes.paper}>
                <Grid container spacing={16} alignItems='center' justify='center'>
                    <Grid item xs={ 12 }>
                        <SearchInputComponent></SearchInputComponent>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
}

SearchForm.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchForm);