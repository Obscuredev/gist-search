import React, { Component } from 'react';
import SearchInput from './SearchInput';
import EachResult from '../EachResult/EachResult';

class SearchInputComponent extends Component{

    state = {

        results : []

    }

    validateInput = () => {

        let githubUserRegex =   /^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i,
            searchInput     =   document.getElementById( 'search-username' ).value.trim(),
            isValidUser     =   githubUserRegex.test( searchInput );

        // Check for empty input 
        if( searchInput.length === 0 ){

            window.alert( 'Please enter something.' );
            return false;

        } 
        // Check for valid github username
        else if( !isValidUser ){

            window.alert( 'Invalid Github Username' );
            return false

        }   
        // Input is valid 
        else {

            this.getGists( searchInput );

        }
    }

    getGists = userQuery => {

        fetch( `https://api.github.com/users/${userQuery}/gists?access_token=5f519e6332e7fc1289e5e2f8748f3e5ee902cc39` )
            .then( response => {
                if (response.status !== 200 ){

                    window.alert( response.status +  ' Error' );
                    return false;

                }
                else {
                    return response.json();
                }
            } )
            .then( data => {

                if( data.length === 0 ){

                    window.alert( `No public gists for ${ userQuery }` );

                }
                else {
                    
                    return data;

                }
            })
            .then( data => {

                for( let i = 0; i<data.length; i++ ){

                    fetch( data[i].forks_url + '?access_token=5f519e6332e7fc1289e5e2f8748f3e5ee902cc39' )
                        .then( response => {
                            
                            return response.json();

                        } )
                        .then( forks => {
                            
                            data[i].user_forks = [];

                            if( forks.length > 0 ){

                                for( let k = 0; k < ( forks.length > 3 ? 3 : forks.length  ); k++ ){

                                    data[i].user_forks.push( {

                                        name : forks[k].owner.login,
                                        avatar : forks[k].owner.avatar_url,
                                        fork_url : forks[k].url 
                                        
                                    } );
                                }
                            }
                        } );
                }

                this.setState({
                        
                    results : data

                });

            } )
            .catch( error => {

                window.alert( 'Oops. Something went wrong!' );

            } );
    }

    render(){

        let gists = null;

        if( this.state.results.length > 0 ){

            gists = (

                <div>
                    {
                        
                        this.state.results.map( (gist, index) => {

                            return <EachResult key={ gist.id }  gist={ gist }/>

                        })
                    }
                </div>
            )
        }
        
        return (
            <div>

                <SearchInput validateInput={ () => this.validateInput() }></SearchInput>
                { gists }

            </div>
        );
    }
}

export default SearchInputComponent