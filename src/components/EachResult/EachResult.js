import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
    root: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
        marginTop: theme.spacing.unit * 3,
    }),
    chip: {
        margin: theme.spacing.unit,
    }
});

function EachResult(props) {

    const { classes } = props;

    let fileTypes = props.gist.files,
        uniqueFileTypes = [];

    for ( let key in fileTypes ){

        uniqueFileTypes.push( fileTypes[key].language );

    }

    uniqueFileTypes =    uniqueFileTypes.filter(function(item, pos, self) {
        return self.indexOf(item) === pos;
    });


    let fork_avatar = null;

    if( _.isArray(props.gist.user_forks) && !_.isEmpty( props.gist.user_forks) ){
        
        fork_avatar =   props.gist.user_forks.map(( fork ) => {
                        
            return <Chip avatar={
                        <Avatar src={ fork.avatar }>
                            
                        </Avatar>
                    }
                    key = { fork.name }
                    label={ fork.name }
                    className={classes.chip}
                    component="a"
                    href={ fork.url }
                    clickable
                />      
        });

    }

    return (
        <div>
            <Grid item xs={ 12 } align='left'>
                <Paper className={classes.root} elevation={4}>
                    <Typography variant="headline" component="h5">
                        { props.gist.description }
                    </Typography>
                    {
                        uniqueFileTypes.map(( fileType, index ) => {

                            return <Chip key={ index } label={ fileType } className={classes.chip} />

                        } )
                    }
                    <Typography align='right'>

                        { fork_avatar }

                    </Typography>
                    
                </Paper>
            </Grid>
        </div>
    );
}

EachResult.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EachResult);