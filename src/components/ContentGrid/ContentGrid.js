import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import SearchForm from '../SearchForm/SearchForm';

const styles = theme => ({
  root: {
    flexGrow: 1,
  }
});

function ContentGrid( props ) {
  const { classes } = props;

  return (
    <div className={classes.root}>
        <Grid container spacing={0} justify='center'>

            <SearchForm></SearchForm>
            
        </Grid>
    </div>
  );
}

ContentGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContentGrid);