/**
 * 
 * App Color palette 
 * 
 */

import { createMuiTheme } from '@material-ui/core/styles';

const AppTheme = createMuiTheme({
    palette: {
        primary: {
            light: '#484848',
            main: '#212121',
            dark: '#000000',
            contrastText: '#fff',
        },
        secondary: {
            light: '#5e92f3',
            main: '#1565c0',
            dark: '#003c8f',
            contrastText: '#fff',
        },
    },
});

export default AppTheme;