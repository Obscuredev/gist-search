/**
 * 
 * Root File
 * 
 */

import React, { Component } from 'react';
import './App.css';

import { MuiThemeProvider } from '@material-ui/core/styles';
import AppTheme from './AppTheme';

import TitleBar from './components/TitleBar/TitleBar';
import ContentGrid from './components/ContentGrid/ContentGrid';

class App extends Component {

    render() {
        return (
            <MuiThemeProvider theme={ AppTheme }>
                <TitleBar/>
                <ContentGrid></ContentGrid>
            </MuiThemeProvider>
        );
    }
}

export default App;
